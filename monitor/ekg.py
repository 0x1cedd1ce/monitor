import mraa
import threading
import time
import scipy.signal as signal
import sched
import multiprocessing

class EKG(threading.Thread):
    NUMBER_OF_INPUT_PINS = 6

    def __init__(self, queue, stop_event):
        threading.Thread.__init__(self)
        #multiprocessing.Process.__init__(self)
        self.queue = queue
        self.stop_event = stop_event
        self.inputPins = [mraa.Aio(i) for i in range(self.NUMBER_OF_INPUT_PINS)];
        self.inputValues = [[] for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.last_time = time.time()
        self.new_value = time.time()
        self.y0 = [0 for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.y1 = [0 for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.y2 = [0 for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.fs = 285
        #self.buffer = [[] for i in range(self.NUMBER_OF_INPUT_PINS)]
        #self.sched = sched.scheduler(time.time, time.sleep)
        self.daemon = True

    def read(self):
        for i, pin in enumerate(self.inputPins):
            self.inputValues[i] = [pin.readFloat()]
        #self.last_time = time.time()

    def print(self):
        for i, value in enumerate(self.inputValues):
            print(i, value)

    def init(self):
        self.b1, self.a1 = self.design_filter(0.4, 70, self.fs, 3, 'bandpass')
        self.b2, self.a2 = self.design_filter(45, 55, self.fs, 3, 'bandstop')
        self.z1 = [signal.lfilter_zi(self.b1, self.a1) for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.z2 = [signal.lfilter_zi(self.b2, self.a2) for i in range(self.NUMBER_OF_INPUT_PINS)]
        self.count = 0
        self.subfs = []

    def run(self):
        self.init()
        #self.sched.enter(0.002, 1, self.loop_once, ())
        #self.sched.run()
        t = time.time()+0.0001
        while not self.stop_event.is_set():
            nt = time.time()
            if nt > t:
                t = nt+0.0001
                self.loop_once()

    def loop_once(self):
        #self.sched.enter(0.002, 1, self.loop_once, ())
        self.read()
        self.count = self.count + 1
        self.last_time = time.time()
        self.subfs.append(1.0 / (self.last_time-self.new_value))
        self.new_value = self.last_time
        if self.count == 1000:
            fs = int(sum(self.subfs) / len(self.subfs))
            #print(fs)
            self.subfs = []
            self.count = 0
            if abs(fs - self.fs) > 10:
                self.fs = fs
                self.init()
        buf = []
        for i, y in enumerate(self.inputValues):
            self.y1[i], self.z1[i] = signal.lfilter(self.b1, self.a1, y, zi=self.z1[i])
            self.y2[i], self.z2[i] = signal.lfilter(self.b2, self.a2, self.y1[i], zi=self.z2[i])
            self.y0[i] = self.inputValues[i]
            self.inputValues[i] = []
            buf.append((self.y0[i][0], self.y1[i][0], self.y2[i][0]))
        #self.queue.put(buf)
        #self.new_value = time.time()
        return buf

    def design_filter(self, lowcut, highcut, fs, order=3, btype='bandpass'):
        nyq = 0.5*fs
        low = lowcut/nyq
        high = highcut/nyq
        b, a = signal.butter(order, [low, high], btype, False)
        return b, a
