#!/usr/bin/env python3
import ekg
import time
import asyncore
import socket
import ssl
import threading
import socketserver
import queue
import bluetooth
import base64
from Crypto.Cipher import AES,Blowfish
from Crypto.PublicKey import RSA
from math import log
import os
from key import d,n

class MonitorServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
    def __init__(self, server_address, RequestHandler, certfile, keyfile, sock_list, stop_event, crypter):
        socketserver.UDPServer.__init__(self, server_address, RequestHandler)
        self.crypter = crypter
        self.ssl_proto = ssl.PROTOCOL_TLSv1
        self.certfile = certfile
        self.keyfile = keyfile
        self.sock_list = sock_list
        self.stop_event = stop_event

    def finish_request(self, request, client_address):
        self.RequestHandlerClass(request, client_address, self, self.sock_list, self.stop_event, self.crypter)

class BluetoothServer(MonitorServer):
    def __init__(self, server_address, RequestHandler, certfile, keyfile, sock_list, stop_event):
        socketserver.BaseServer.__init__(self, server_address, RequestHandler)
        self.socket = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
        self.server_bind()
        self.server_activate()
        self.ssl_proto = ssl.PROTOCOL_TLSv1
        self.certfile = certfile
        self.keyfile = keyfile
        self.sock_list = sock_list
        self.stop_event = stop_event


class MonitorHandler(socketserver.BaseRequestHandler):
    def __init__(self, request, client_address, server, sock_list, stop_event, crypter):
        self.sock_list = sock_list
        self.stop_event = stop_event
        self.crypter = crypter
        socketserver.BaseRequestHandler.__init__(self, request, client_address, server)

    def handle(self):
        for item in self.sock_list:
            if item[0][1] == self.client_address:
                return
        print("handling")
        event = threading.Event()
        event.clear()
        handshake((self.request[1],self.client_address),self.crypter)
        self.sock_list.append(((self.request[1],self.client_address),event))
        self.request[1].settimeout(2.0)
        s = b""
        while not s.startswith(b"STOP") and not event.is_set():
            try:
                s = self.request[1].recv(1024)
                if s.startswith(b"START"):
                    handshake((self.request[1],self.client_address),self.crypter)
                if s != None and s != "":
                    print(s)
            except socket.timeout as e:
                pass
        print("stop")
        event.set()

def to_bytes(i):
    if i > 1.0:
        return b"\x7f"
    if i < -1.0:
        return b"\x80"
    return (int(i*127)).to_bytes(1, 'big', signed=True)

def stream_data(sock_list, stop_event, pipe, crypter):
    count = 0
    line = b""
    ekgReader.init()
    while not stop_event.is_set():
        try:
            #data = pipe.recv()
            #data = pipe.get()
            data = ekgReader.loop_once()
            #line = b"EKG" + to_bytes(data[0][0]) + to_bytes(data[0][1]) + to_bytes(data[0][2]) + b"\n"
            line = "EKG {} {} {}\n".format(*data[0]).encode('ascii')
            #print(line)
            line = crypter.encrypt(line) + b"\n"
            count += 1
        except Exception as e:
            print(e)
            for sock, event in sock_list:
                sock.close()
        else:
            if count == 1:
                count = 0
                for sock, event in sock_list:
                    try:
                        if not event.is_set():
                            if type(sock) == tuple:
                                sock[0].sendto(line,sock[1])
                            else:
                                sock.sendall(line)
                        else:
                            print("remove", sock)
                            sock_list.remove((sock,event))
                    except Exception as e:
                        print(e)
                        if (sock,event) in sock_list:
                            sock_list.remove((sock,event))
                        event.set()
                line = b""

def serve_bluetooth(server_address, sock_list, stop_event,crypter):
    sock = bluetooth.BluetoothSocket(bluetooth.L2CAP)
    sock.bind(server_address)
    uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
    sock.listen(1)
    bluetooth.advertise_service(sock, "Monitor", uuid, service_classes = [ uuid, bluetooth.SERIAL_PORT_CLASS], profiles = [bluetooth.SERIAL_PORT_PROFILE])
    while not stop_event.is_set():
        try:
            client, address = sock.accept()
            print(client, address)
            event = threading.Event()
            event.clear()
            handshake(client,crypter)
            sock_list.append((client,event))
        except Exception as e:
            print(e)
    print("closing bluetooth")
    sock.close()

def init_bluetooth():
    pass

def handshake(socket, crypter):
    sessionopen = b"KEY" + base64.b64encode(crypter.encrypt_sessionkey()) + b"\n"
    print(crypter.encrypt_sessionkey())
    print(sessionopen)
    if(type(socket) == tuple):
        socket[0].sendto(sessionopen, socket[1])
    else:
        socket.sendall(sessionopen)

class Crypter:
    def __init__(self, d, n):
        self.exp = d
        self.mod = n
        self.sessionkey = os.urandom(32)
        print(int.from_bytes(self.sessionkey, "big"), len(self.sessionkey))
        self.key = AES.new(self.sessionkey, AES.MODE_ECB)
        #self.key = Blowfish.new(str(n)[:10], Blowfish.MODE_ECB)
        self.rsa = RSA.construct((d,n))

    def decrypt(self, line):
        line = base64.b64decode(line)
        line = self.key.decrypt(line)
        return line + b"\n"

    def encrypt(self,line):
        line = self.key.encrypt(line+(b" "*(16-(len(line)%16))))
        line = base64.b64encode(line)
        return line + b"\n"

    def encrypt_sessionkey(self):
        #return self.rsa.encrypt(self.sessionkey, None)[0]
        i = pow(int.from_bytes(self.sessionkey, "big"), self.exp, self.mod)
        r = i.to_bytes(int(log(i)/log(2))//8 + 1, "big")
        print(int.from_bytes(self.sessionkey, "big"))
        print(i, len(r))
        return r

import sys
if __name__ == "__main__":

    q = queue.Queue(10)
    stop_event = threading.Event()
    sock_list = []
    stop_event.clear()
    ekgReader = ekg.EKG(q, stop_event)
    bluetooth_mac = "43:34:1B:00:1F:AC"
    #bserver = BluetoothServer((bluetooth_mac, 2), MonitorHandler, sys.argv[3], sys.argv[4], sock_list, stop_event)
    #ekgReader.start()
    crypter = Crypter(d, n)
    streamer = threading.Thread(target=stream_data, args=(sock_list, stop_event, q, crypter))
    server = MonitorServer((sys.argv[1], int(sys.argv[2])), MonitorHandler, sys.argv[3], sys.argv[4], sock_list, stop_event, crypter)
    streamer.daemon = True
    streamer.start()
    t_server = threading.Thread(target=serve_bluetooth, args=(("",bluetooth.PORT_ANY),sock_list, stop_event,crypter))
    t_server.daemon = True
    t_server.start()
    try:
        server.serve_forever()
    except:
        pass
    finally:
        print("shutdown")
        server.shutdown()
        stop_event.set()
