#include <interrupt.c>
#include <math.h>
#include <bt/bluetooth.c>

#define LED2_ON		TRISD &= (~0x0002); LATD |= 0x0002;
#define LED2_OFF		TRISD &= (~0x0002); LATD &= (~0x0002);
#define LED2_TOGG	TRISD &= (~0x0002); LATD ^= 0x0002;

#define btp UART2
#define btbaud 115200

#define LED1PIN		13
#define CAL_SIG 		9
#define NUMCHANNELS 	2
#define LOW_N 5
#define N 8

const double coef[11] = {-0.08391608, 0.02097902, 0.1025641, 0.16083916, 0.1958042, 0.20745921, 0.1958042, 0.16083916, 0.1025641, 0.02097902, -0.08391608};

// Global constants and variables
double inputBuffer[NUMCHANNELS][N]; // buffer for samples
double TXBuffer[NUMCHANNELS];  //The transmission packet
double lowpassBuffer[NUMCHANNELS][LOW_N];

volatile unsigned char counter = 0;
volatile unsigned char low_counter = 0;
volatile unsigned char cdc_output = TRUE;

/****************************************************/
/*  Function name: toggle_LED1        */
/*  Parameters                        */
/*    Input   :  No 	                 */
/*    Output  :  No                   */
/*    Action: Switches-over LED1.     */
/****************************************************/

void toggle_LED1(void){
 if(digitalRead(LED1PIN) == HIGH) {
    digitalWrite(LED1PIN, LOW);
 } else {
    digitalWrite(LED1PIN, HIGH);
 }
}


/****************************************************/
/*  Function name: toggle_GAL_SIG     */
/*  Parameters                        */
/*    Input   :  No	                   */
/*    Output  :  No                   */
/*    Action: Switches-over GAL_SIG.  */
/****************************************************/

void toggle_GAL_SIG(void){
 if(digitalRead(CAL_SIG) == HIGH) {
    digitalWrite(CAL_SIG, LOW);
 } else {
    digitalWrite(CAL_SIG, HIGH);
 }
}

void ReadInterrupt(void) __attribute__ ((interrupt));
// Tmr1Interrupt is declared as an interrupt routine

void ISR_wrapper_vector_4(void) __attribute__ ((section (".vector_4")));
// Put the ISR_wrapper in the good place

void ISR_wrapper_vector_4(void) { ReadInterrupt(); }
// ISR_wrapper will call SendInterrupt

// Send Packet
void Send() {
    char* buf[256];
    sprintf(buf, "%f %f %f %f %f %f",  TXBuffer[0],TXBuffer[1],TXBuffer[2],TXBuffer[3],TXBuffer[4],TXBuffer[5]);
    CDC.println(buf);
}
void DebugSend() {
    char* buf[256];
    sprintf(buf, "EKG %f %f\n", inputBuffer[1][counter], TXBuffer[1]);
    if(cdc_output == 1) {
        CDC.printf(buf);
     }
    SerialFlush(btp);
    SerialPrintf(btp, buf);
    //Delayms(BT_DELAY);
}

void SendInterrupt(void) {
    if(IFS0bits.T4IF) {
        TMR4=0;			// reset the timer register
        IFS0CLR=TIMER4_INT_ENABLE;
        //asm("DI");
        DebugSend();
        //asm("EI");
    }
}

void SavitzkyGolayFilter()
{
    unsigned char i;
    unsigned char CCh;
    unsigned char c;
    for(CCh=0;CCh<NUMCHANNELS;CCh++) {
        TXBuffer[CCh] = 0;
        for(i=1;i<=N;i++) {
            c = counter + i;
            if(c >= N) {
                c = c - N;
            }
            TXBuffer[CCh] += inputBuffer[CCh][c] * coef[i];
        }
    }
}

/* Digital filter designed by mkfilter/mkshape/gencode   A.J. Fisher
   Command line: /www/usr/fisher/helpers/mkfilter -Bu -Bp -o 10 -a 2.2333333333e-03 4.6666666667e-01 -l */

#define NZEROS 10
#define NPOLES 10
#define GAIN   1.304382734e+00

double xv[NUMCHANNELS][NZEROS+1], yv[NUMCHANNELS][NPOLES+1];

double ButterworthBandPass(unsigned char CCh, double new_input) {
    xv[CCh][0] = xv[CCh][1]; xv[CCh][1] = xv[CCh][2]; xv[CCh][2] = xv[CCh][3]; xv[CCh][3] = xv[CCh][4]; xv[CCh][4] = xv[CCh][5];
    xv[CCh][5] = xv[CCh][6]; xv[CCh][6] = xv[CCh][7]; xv[CCh][7] = xv[CCh][8]; xv[CCh][8] = xv[CCh][9]; xv[CCh][9] = xv[CCh][10]; 
    xv[CCh][10] = new_input / GAIN;
    yv[CCh][0] = yv[CCh][1]; yv[CCh][1] = yv[CCh][2]; yv[CCh][2] = yv[CCh][3]; yv[CCh][3] = yv[CCh][4]; yv[CCh][4] = yv[CCh][5];
    yv[CCh][5] = yv[CCh][6]; yv[CCh][6] = yv[CCh][7]; yv[CCh][7] = yv[CCh][8]; yv[CCh][8] = yv[CCh][9]; yv[CCh][9] = yv[CCh][10]; 
    yv[CCh][10] =   (xv[CCh][10] - xv[CCh][0]) + 5 * (xv[CCh][2] - xv[CCh][8]) + 10 * (xv[CCh][6] - xv[CCh][4])
                     + (  0.1973567132 * yv[CCh][0]) + (  0.3312157166 * yv[CCh][1])
                     + ( -1.0829392924 * yv[CCh][2]) + ( -1.7449485605 * yv[CCh][3])
                     + (  2.5668208833 * yv[CCh][4]) + (  3.6105453681 * yv[CCh][5])
                     + ( -3.4238893763 * yv[CCh][6]) + ( -3.5228516931 * yv[CCh][7])
                     + (  2.6654165379 * yv[CCh][8]) + (  1.4032737015 * yv[CCh][9]);
    return yv[CCh][10];
}

#define NZEROS1 6
#define NPOLES1 6
#define GAIN1   1.110452651e+00

double xv1[NUMCHANNELS][NZEROS+1], yv1[NUMCHANNELS][NPOLES+1];

double ButterworthBandStop(unsigned char CCh, double new_input)
{
    xv1[CCh][0] = xv1[CCh][1]; xv1[CCh][1] = xv1[CCh][2]; xv1[CCh][2] = xv1[CCh][3]; xv1[CCh][3] = xv1[CCh][4]; xv1[CCh][4] = xv1[CCh][5]; xv1[CCh][5] = xv1[CCh][6]; 
    xv1[CCh][6] = new_input / GAIN;
    yv1[CCh][0] = yv1[CCh][1]; yv1[CCh][1] = yv1[CCh][2]; yv1[CCh][2] = yv1[CCh][3]; yv1[CCh][3] = yv1[CCh][4]; yv1[CCh][4] = yv1[CCh][5]; yv1[CCh][5] = yv1[CCh][6]; 
    yv1[CCh][6] =   (xv1[CCh][0] + xv1[CCh][6]) -   1.4950895407 * (xv1[CCh][1] + xv1[CCh][5]) +   3.7450975782 * (xv1[CCh][2] + xv1[CCh][4])
                     -   3.1139554809 * xv1[CCh][3]
                     + ( -0.7776385602 * yv1[CCh][0]) + (  1.2113083143 * yv1[CCh][1])
                     + ( -3.1577277729 * yv1[CCh][2]) + (  2.7390466637 * yv1[CCh][3])
                     + ( -3.4334502677 * yv1[CCh][4]) + (  1.4325040482 * yv1[CCh][5]);
    return yv1[CCh][6];
}

void ReadInterrupt(void)
{
    if(IFS0bits.T1IF) {
        TMR1=0;			// reset the timer register
        IFS0CLR=TIMER1_INT_ENABLE;
        asm("DI");
        
        toggle_LED1();
        unsigned char CCh;
        double value;
        for(CCh=1;CCh<NUMCHANNELS;CCh++) {
            inputBuffer[CCh][counter] = (double) analogRead(CCh) / 1024.0;
            value = ButterworthBandPass(CCh, inputBuffer[CCh][counter]);
            TXBuffer[CCh] = ButterworthBandStop(CCh, value);
            //TXBuffer[CCh] = yv[CCh][20];
        }
        //inputBuffer[0][counter] = SerialRead(UART2);
        //SavitzkyGolayFilter();
        
        DebugSend();
        counter++;
        if(counter == N) {
            toggle_GAL_SIG();	// Generate CAL signal with frequ 1Hz
            counter = 0;
        }
        asm("EI");
    }
}

// configure timer 1 
void init_timer1(void)
{
    IntConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);	// interrupt mode (interrupt.c)
    T1CON=0;		// reset timer 1 configuration
    TMR1=0;			// reset timer 1 counter register
    //PR1=625;		// define the preload register (250 Hz)
    PR1=675;
    IPC1SET=INT_PRIORITY_7;		// select interrupt priority and sub-priority
    IFS0CLR=TIMER1_INT_ENABLE;		// clear interrupt flag
    IEC0SET=TIMER1_INT_ENABLE;		// enable timer 1 interrupt
    T1CONSET=0x8030;	// start timer 1 and set prescaler
}

// TMR4 Init Function
void InitTmr4(void)
{
 IntConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);	// interrupt mode (interrupt.c)
 T4CON=0;		            // reset timer 4 configuration
 TMR4=0;			            // reset timer 4 Counter register (VERY DANGEROUS OPERATION USE ONLY AT INIT TIME)
 PR4=6250-1;		         // define the preload register
 IPC4SET=0x7;		         // select interrupt priority and sub-priority
 IFS0CLR=0x10000;		    // clear interrupt flag
 IEC0SET=0x10000;		    // enable timer 4 interrupt
 T4CONSET=0x8070;	      // start timer 4 and set prescaler to 256
}

/*********************************************************/
/*  Function name: setup              		  */
/*  Parameters                        		  */
/*    Input   :  No	                 		   */
/*    Output  :  No                   		  */
/*    Action: Initializes all peripherals */
/*********************************************************/
void setup()
{
  pinMode(LED1PIN, OUTPUT);
  pinMode(CAL_SIG, OUTPUT);

  
  local_init_bt(btp, btbaud);
				
		//Clear the buffer of the Serial Communication
		SerialFlush(btp);
  
  init_timer1();
  //init_timer4();
}


/*********************************************************/
/*  Function name: loop                   */
/*  Parameters                            */
/*    Input   :  No	                       */
/*    Output  :  No                       */
/*    Action: Do nothing.						         */
/*********************************************************/
void loop()
{
//reads every received character from the BT module
		//Also, the characters '1' and '2' toggle the two LEDS on the board
		
		char comm;
		
		//waits until a character is received and stores it
		comm = SerialGetKey(btp);
		
		//Uncomment the next row if you want the board to echo characters on the terminal

		
		// A simple switch that performs different commands
		// You can make this into a function for more
		// sophisticated examples
		
		switch(comm)
		{
			case '1': 
        if(cdc_output == 1) {
          cdc_output = 0;
        } else {
          cdc_output = 1;
        }
				break;
			default:
				CDC.printf("Unknown command\n");
				
		}	
}


void local_init_bt(unsigned char uart_port, uint32_t baud_rate)
{
	//configure the board to open a serial connection to the MOD-BT module (via UEXT)
	SerialConfigure(uart_port, UART_ENABLE, UART_RX_TX_ENABLED,	baud_rate);

  //BT_sendCommand(uart_port,"AT+BTPAR=0");
  //BT_reset(uart_port);
  //BT_restore(uart_port);
  	
	//Disable the echo
	BT_echoOff(uart_port);
		
	//Allow other devices to auto-connect to MOD-BT
	BT_setAutoConnection(uart_port);
	
	//Start the server
	BT_start(uart_port);

}

