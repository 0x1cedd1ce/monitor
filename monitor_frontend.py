# coding: utf8
import sys
import time
from PyQt4.Qt import QApplication,QGraphicsView,QGraphicsScene,QGraphicsPixmapItem,QGraphicsObject
from PyQt4.QtCore import QRectF
from PyQt4.QtGui import QPen,QColor
from threading import Thread
from scipy import arange, fft, log10, ifft, array, real, zeros
import traceback
from math import cos,pi,factorial,log
import numpy as np
from scipy.signal import butter, lfilter, filtfilt
import asyncore
import asynchat
import socket
import ssl
import bluetooth
import base64
from Crypto.Cipher import AES,Blowfish
from Crypto.PublicKey import RSA
from key import e,d,n

class SynchronMonitorClient(Thread):
    def __init__(self, sock, host, ekg_dialog, crypter):
        Thread.__init__(self)
        self.ekg_dialog = ekg_dialog
        self.host = host
        self.ibuffer = b""
        self.obuffer = b""
        self.sock = sock
        sock.settimeout(5)
        self.crypter = crypter

    def run(self):
        self.crypter.sessionkey = None
        print(self.host)
        self.sock.sendto(b"START", (self.host, 50007))
        while self.ekg_dialog.running:
            try:
                self.ibuffer += self.sock.recv(4096)
                lines = self.ibuffer.split(b"\n")
                lines.reverse()
                while len(lines) > 1:
                    if(self.crypter.sessionkey):
                        line = self.crypter.decrypt(lines.pop())
                        print(line)
                        self.ekg_dialog.set_line(line)
                    else:
                        if(lines[-1][0:3] == b"KEY"):
                            self.crypter.set_sessionkey(lines.pop()[3:])
                            self.sock.sendto(b"EKG", (self.host, 50007))
                        else:
                            #lines.pop()
                            print(lines.pop())
                self.ibuffer = lines[0]
            except socket.timeout:
                pass
            except Exception as e:
                raise e
        self.sock.sendto(b"STOP", (self.host, 50007))
        print("stop")

class EKGDialog(QGraphicsObject):
    def __init__(self):
        super(EKGDialog, self).__init__()
        self.label = ""
        self.spectrum = ""
        try: 
            self.running = False
            self.thread = Thread(target=self.read)
            self.sample_size = 2048
            self.points = zeros(self.sample_size)
            self.points_fft = zeros(self.sample_size)
            self.points_flt = zeros(self.sample_size)
            self.points_mod = zeros(self.sample_size)
            self.points_mod_fft = zeros(self.sample_size)
            self.pen1 = QPen(QColor("black"))
            self.penR = QPen(QColor("red"))
            self.penB = QPen(QColor("blue"))
            self.i = 0
            self.xh = 0.9
            self.xl = 0.6
            self.al0 = 1-self.xl
            self.ah0 = (1+self.xh)/2
            self.ah1 = -self.ah0
            self.bl0 = self.xl
            self.bh0 = self.xh
            self.input1 = array([0.0,0.0])
            self.output1 = 0.0
            self.input2 = 0.0
            self.output2 = 0.0
            self.thread.start()
        except Exception as e:
            sys.exit(0)
            print("/dev/ttyACM0 not ready", e)
    
    def boundingRect(self):
        return QRectF(0, 0, 1024, 512)

    def paint(self, painter, option, widget):
        painter.setPen(self.pen1)
        painter.drawRect(0, 0, 1024, 512)
        painter.drawLine(0, 256, 1024, 256)
        painter.drawText(10, 10, self.label)
        painter.drawText(10, 500, self.spectrum)
        painter.drawLine(self.i*(1024/self.sample_size), 0, self.i*(1024/self.sample_size), 512)
        old_x = 0
        old_y = 256
        painter.setPen(self.pen1)
        for x,y in enumerate(self.points):
            if (x == self.i) or (x == self.i+1):
                old_x = x*(1024/self.sample_size)
                old_y = y
                continue
            x = x*(1024/self.sample_size)
            y = y-128
            try:
                painter.drawLine(old_x,old_y,x,y)
            except Exception as e:
                pass
            old_x = x
            old_y = y
        old_x = 0
        old_y = 256
        painter.setPen(self.penB)
        for x,y in enumerate(self.points_fft):
            x = x*(1024/self.sample_size)
            y = y+256
            try :
                painter.drawLine(old_x,old_y,x,y)
            except Exception as e:
                pass
            old_x = x
            old_y = y
        old_x = 0
        old_y = 256.0
        painter.setPen(self.penR)
        for x,y in enumerate(self.points_flt):
            x = x*(1024/self.sample_size)
            y = y+128
            try:
                painter.drawLine(old_x,old_y,x,y)
            except Exception as e:
                pass
            old_x = x
            old_y = y

    def read(self):
        last_time = time.clock()
        while(self.running):
            try:
                for line in self.serial:
                    if not self.running:
                        return
                    self.set_line(line)
            except Exception as e:
                print(e)

    def set_line(self, line):
        last_time = time.time()
        if(len(line.strip())>0 and line.startswith(b"EKG")):
            print(line)
            try:
                self.points[self.i] = float(line.split()[1])*512    # schwarz
                self.points_fft[self.i] = float(line.split()[2])*512   # blau
                self.points_flt[self.i] = float(line.split()[3])*512   # rot
                #self.points_mod[self.i] = float(line.split()[1]);
                #self.points_mod_fft[self.i] = float(line.split()[4]);
                self.i+=1
                if(self.i==self.sample_size):
                    new_time = time.time()
                    Fs = self.sample_size/(new_time-last_time)
                    self.label = str(Fs) + " " + str(new_time-last_time)
                    last_time = new_time
                    self.i = 0
                    #self.fft(Fs)
                    #self.points_flt = self.savitzky_golay(self.points, 11, 7)
                    #self.points_flt = self.fft(self.points_flt,Fs, 0.05, 70)
                    #self.points_fft = self.fft(self.points_mod,Fs, 0.40, 70)
                    #self.points_fft = self.savitzky_golay(self.points, 13, 5)
                #self.update(QRectF(0,-512,1024,1024)) 
            except Exception as e:
                traceback.print_exc()
            finally:
                self.update(QRectF(0,0,1024,512))

    def savitzky_golay(self, y, window_size, order, deriv=0, rate=1):
        try:
            window_size = np.abs(np.int(window_size))
            order = np.abs(np.int(order))
        except ValueError as msg:
            raise ValueError("window_size and order have to be of type int")
        if window_size % 2 != 1 or window_size < 1:
            raise TypeError("window_size size must be a positive odd number")
        if window_size < order + 2:
            raise TypeError("window_size is too small for the polynomials order")
        order_range = range(order+1)
        half_window = (window_size -1) // 2
        # precompute coefficients
        b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
        #print(b)
        m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
        #print(m)
        # pad the signal at the extremes with
        # values taken from the signal itself
        firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
        lastvals = y[-1] + np.abs( y[-half_window-1:-1][::-1] - y[-1])
        y = np.concatenate((firstvals, y, lastvals))
        #print(y)
        return np.convolve( m[::-1], y, mode='valid')
        #print(self.points)
        #print(self.points_fft)

    def fft(self,y,Fs,lowFreq,highFreq):
        n = self.sample_size
        k = arange(n)
        T = n/Fs
        frq = k/T
        f = frq>=highFreq
        f += frq<=lowFreq
        Y = fft(y)
        f[0] = 0
        Y = Y*(1-f);
        return real(ifft(Y))


class Crypter:
    def __init__(self, e, d, n):
        self.exp = e
        self.mod = n
        self.key = AES.new(str(n)[:16], AES.MODE_ECB)
        self.rsa = RSA.construct((n,e))

    def crypt(self, line):
        line = self.key.encrypt(line + (b"="*(16-(len(line)%16))))
        line = base64.b64encode(line)
        return line

    def decrypt(self, line):
        line = base64.b64decode(line)
        line = self.key.decrypt(line)
        return line
    
    def set_sessionkey(self, key):
        print(key)
        key = base64.b64decode(key)
        print(key)
        key = int.from_bytes(key,"big")
        print(key)
        i = pow(key, self.exp, self.mod)
        self.sessionkey = i.to_bytes(int(log(i)/log(2))//8 + 1, "big")
        print(i)
        self.key = AES.new(self.sessionkey, AES.MODE_ECB)

if __name__ == "__main__":
    crypter = Crypter(e,d,n)
    app = QApplication(sys.argv)
    #scene = QGraphicsScene()
    scene2 = QGraphicsScene()
    #ekg_dialog = EKGDialog()
    ekg_dialog2 = EKGDialog()
    ekg_dialog2.running = True
    #ekg_dialog.running = True
    uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
    if len(sys.argv) == 4:
        host = sys.argv[1]
        port = int(sys.argv[2])
        service_matches = bluetooth.find_service( uuid = uuid, address = host) 
    else:
        service_matches = bluetooth.find_service( uuid = uuid )

    if len(service_matches) == 0:
        print("couldn't find the FooBar service")
        #sys.exit(0)
    else:
        first_match = service_matches[0]
        port = first_match["port"]
        name = first_match["name"]
        host = first_match["host"]  
    
    #btsock = bluetooth.BluetoothSocket(bluetooth.L2CAP)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #ssl_sock = ssl.wrap_socket(sock, ca_certs=sys.argv[2], cert_reqs=ssl.CERT_REQUIRED)
    print(host, port)
    #btsock.connect((host, port))
    #sock.connect((host, 50007))
    #ssl_sock = ssl.wrap_socket(sock, ca_certs=sys.argv[3], server_side=False, do_handshake_on_connect=True, cert_reqs=ssl.CERT_REQUIRED)
    #scene.addItem(ekg_dialog)
    scene2.addItem(ekg_dialog2)
    #window = QGraphicsView(scene)
    #window.show()
    window2 = QGraphicsView(scene2)
    window2.show()
    #bt = SynchronMonitorClient(btsock, sys.argv[1], ekg_dialog, crypter)
    t = SynchronMonitorClient(sock, sys.argv[1], ekg_dialog2, crypter)
    t.start()
    #bt.start()
    app.exec_()
    print("exit")
    #ekg_dialog.running = False
    ekg_dialog2.running = False
