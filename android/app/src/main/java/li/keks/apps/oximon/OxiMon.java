/*
* Copyright 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


package li.keks.apps.oximon;

import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;

import li.keks.apps.oximon.activities.SampleActivityBase;
import li.keks.apps.oximon.connector.BlueManager;
import li.keks.apps.oximon.connector.IDevice;
import li.keks.apps.oximon.ecg.EcgFragment;
import li.keks.apps.oximon.logger.Log;
import li.keks.apps.oximon.logger.LogFragment;
import li.keks.apps.oximon.logger.LogWrapper;
import li.keks.apps.oximon.logger.MessageOnlyLogFilter;

/**
 * A simple launcher activity containing a summary sample description
 * and a few action bar buttons.
 */
public class OxiMon extends SampleActivityBase {

    public static final String TAG = "MainActivity";
    public static final String FRAGTAG = "ImmersiveModeFragment";
    public static final String ECGFRAGTAG = "EcgFragment";
    public static int REQUEST_ENABLE_BT = 10;
    private BlueManager mBluetoothManager;
    private boolean mBluetoothEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getFragmentManager().findFragmentByTag(FRAGTAG) == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            ImmersiveModeFragment fragment = new ImmersiveModeFragment();
            transaction.add(fragment, FRAGTAG);
            transaction.commit();
        }
    }

    public void openECGView(IDevice device) {
        EcgFragment fragment = (EcgFragment) getFragmentManager().findFragmentById(R.id.ecg_fragment);
        fragment.setDevice(device);
        //FragmentTransaction transaction = getFragmentManager().beginTransaction();
        //transaction.add(R.id.ecg_fragment, fragment);
        //transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Create a chain of targets that will receive log data
     */
    @Override
    public void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        LogFragment logFragment = (LogFragment) getFragmentManager()
                .findFragmentById(R.id.log_fragment);
        msgFilter.setNext(logFragment.getLogView());
        logFragment.getLogView().setBackgroundColor(Color.WHITE);


        Log.i(TAG, "Ready");
    }

    public BlueManager enableBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            mBluetoothManager = new BlueManager(mBluetoothAdapter);
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
        return mBluetoothManager;
    }

}
