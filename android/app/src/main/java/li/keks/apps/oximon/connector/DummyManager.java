package li.keks.apps.oximon.connector;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by ich on 29.12.15.
 */
public class DummyManager extends BaseAdapter {

    DummyDevice device = new DummyDevice();

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return device;
    }

    @Override
    public long getItemId(int position) {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = new TextView(parent.getContext());
        view.setText(device.id() + "");
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }
}
