package li.keks.apps.oximon.connector;

public interface DeviceListener {
    void setECG(float value);
}
