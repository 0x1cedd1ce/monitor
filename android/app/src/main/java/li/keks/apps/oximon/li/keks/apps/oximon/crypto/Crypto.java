package li.keks.apps.oximon.li.keks.apps.oximon.crypto;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import li.keks.apps.oximon.logger.Log;

/**
 * Created by ich on 23.08.15.
 */
public class Crypto {

    private static String TAG = "Crypto";
    private static Cipher aes;
    private static Cipher rsa;
    private static CryptoKey key;
    private static boolean isInit = false;

    static {
        try {
            aes = Cipher.getInstance("AES/ECB/NoPadding");
            rsa = Cipher.getInstance("RSA");
            key = new CryptoKey();
            rsa.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (NoSuchPaddingException e1) {
            e1.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public static byte[] decryptKey(byte[] encryptedData) {
        byte[] sessionkey = new byte[0];
        try {
            BigInteger k = new BigInteger(encryptedData);
            sessionkey = rsa.doFinal(encryptedData);
            //BigInteger skey = k.modPow(key.getPublicExponent(), key.getModulus());
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return sessionkey;
    }

    public static boolean hasSessionkey() {
        return isInit;
    }

    public static void setSessionKey(byte[] sessionkey) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(sessionkey, "AES");
        try {
            aes.init(Cipher.DECRYPT_MODE, secretKeySpec);
            isInit = true;
        } catch (InvalidKeyException e) {
            //e.printStackTrace();
        }
        isInit = true;
    }

    public static byte[] decrypt(byte[] encryptedData) {
        try {
            return aes.doFinal(encryptedData);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Log.w(TAG, encryptedData.length + "");
            //e.printStackTrace();
        } catch (IllegalStateException e) {
            Log.w(TAG, e.toString());
        }
        return new byte[]{'!'};
    }

    private static class CryptoKey implements Key, RSAPrivateKey, RSAPublicKey {
        private static BigInteger e = new BigInteger("65537");
        private static BigInteger n = new BigInteger("3772874936713750803385651316740472110473781440786886067519276920738338978451192005612435918242074954340257415155032849543159947307241766282354495077409678947139015974729593291047004514287907977600639464386816573388886434415285751917390179514788972950616611226353976848517650953009250031640229472937709396311938285591766973687903562888411251684118645248426278937225429330241043268831192804737222035572683866030144218822875199450537279181658044203301776706706135742323682584369063560945766023574239015173933747910213911195549380537325141157142552758889569037039961788985504304723400467160173442699588870104052274888231");
        private static BigInteger d = new BigInteger("920695009885454271610643171164843835738700071448260812637682466291839041798829878477191321550353307975094020790918723205879992024119467905972129328059157352359648480764306964061747458641782692002487555944555860936699280491991776102275373315531990240615399901476710129214684707744891218029848634522373413113430009313909916527741848416089023598013604163117136676696139643649439897169238776202655262981412827553266077303362890033337528725516465045096594901488800163338051804897379868007781444452689716724420439606245858059207008596640821255460367758834818465316667352329508818568722708196745840219151423661601728344721");

        public CryptoKey() {
        }

        @Override
        public BigInteger getPrivateExponent() {
            return d;
        }

        @Override
        public BigInteger getPublicExponent() {
            return e;
        }

        @Override
        public String getAlgorithm() {
            return "RSA";
        }

        @Override
        public String getFormat() {
            return null;
        }

        @Override
        public byte[] getEncoded() {
            return null;
        }

        @Override
        public BigInteger getModulus() {
            return n;
        }
    }

}
