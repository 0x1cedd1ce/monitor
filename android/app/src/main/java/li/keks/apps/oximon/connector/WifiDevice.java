package li.keks.apps.oximon.connector;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class WifiDevice extends AbstractDevice implements Comparable<WifiDevice> {

    private static String TAG = "WifiDevice";
    InetAddress mHost;
    int mPort;
    boolean mRunning = false;
    DatagramSocket mSocket = null;
    List<String> mReceiveBuffer = new ArrayList<>();
    Thread mReceiveThread;

    Thread mThread = null;

    public WifiDevice(InetAddress host, int port) {
        mHost = host;
        mPort = port;
    }

    public int getPort() {
        return mPort;
    }

    public InetAddress getHost() {
        return mHost;
    }

    @Override
    public boolean connect() {
        try {
            mSocket = new DatagramSocket(mPort);
            mSocket.connect(mHost, mPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean disconnect() {
        mRunning = false;
        try {
            mThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mSocket.disconnect();
        return false;
    }

    public void sendPacket(byte[] data) {
        DatagramPacket pack = new DatagramPacket(data, data.length);
        pack.setAddress(mHost);
        pack.setPort(mPort);
        try {
            mSocket.send(pack);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startReceiving() {
        this.connect();
        mReceiveThread = new Thread(new Runnable() {
            @Override
            public void run() {
                receiveLine();
            }
        });
        mReceiveThread.start();
    }

    @Override
    public void stopReceiving() {
        mSocket.disconnect();
        try {
            mReceiveThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void receiveLine() {
        byte[] buffer = new byte[4096];
        DatagramPacket pack = new DatagramPacket(buffer, 4096);
        pack.setAddress(mHost);
        pack.setPort(mPort);
        String lastEnd = "";
        while (mSocket.isConnected()) {
            try {
                pack.setData(new byte[4096]);
                mSocket.receive(pack);
                int j = 0;
                for (int i = 0; i < pack.getLength(); i++) {
                    if (pack.getData()[i] == '\n') {
                        String line = lastEnd + new String(pack.getData(), j, i - j);
                        mReceiveBuffer.add(line);
                        lastEnd = "";
                    }
                    if (pack.getData()[i] == 0) {
                        lastEnd = new String(pack.getData(), j, i - j);
                    }
                }
            } catch (IOException ex) {

            }
        }
    }

    @Override
    public synchronized String getLine() {
        while (mReceiveBuffer.size() == 0) {
            // Do Nothing
        }
        return mReceiveBuffer.remove(0);
    }

    private List<String> split(byte[] s, String lastString) {
        List<String> splitStrings = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == '\n') {
                splitStrings.add(new String(s, j, i - j));
                j = i + 1;
            }
            if (s[i] == 0) {
                splitStrings.add(new String(s, j, i - j));
                break;
            }
        }
        splitStrings.set(0, lastString + splitStrings.get(0));
        return splitStrings;
    }

    @Override
    public long id() {
        return mHost.hashCode();
    }

    @Override
    public boolean isConnected() {
        return mSocket != null && mSocket.isConnected();
    }

    @Override
    public int compareTo(WifiDevice another) {
        if (another == null || this.mHost == null || this.mHost.getHostName() == null) {
            throw new InvalidParameterException();
        }
        int ret = this.mHost.getHostName().compareTo(another.getHost().getHostName());
        if (ret == 0) {
            return this.mPort - another.getPort();
        }
        return ret;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof WifiDevice) && ((WifiDevice) o).getHost() == mHost && ((WifiDevice) o).getPort() == mPort;
    }

    @Override
    public String toString() {
        return mHost.getHostAddress() + " : " + mPort;
    }
}
