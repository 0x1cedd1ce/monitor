package li.keks.apps.oximon.connector;

import android.net.Uri;

import wfdb.WFDB_SampleArray;
import wfdb.WFDB_SiginfoArray;
import wfdb.wfdb;

/**
 * Created by ich on 28.12.15.
 */
public class DummyDevice extends AbstractDevice {

    private boolean mIsConnected = false;
    private int channels = 3;
    private WFDB_SiginfoArray siginfos = new WFDB_SiginfoArray(channels);
    private WFDB_SampleArray samples = new WFDB_SampleArray(channels);

    private String path = Uri.parse(
            "file:///android_asset/s0010_re.hea").getPath();

    @Override
    public boolean connect() {
        int ret = wfdb.isigopen(path, siginfos.cast(), channels);
        mIsConnected = ret > 0;
        return mIsConnected;
    }

    @Override
    public boolean disconnect() {
        wfdb.wfdbquit();
        return false;
    }

    @Override
    public long id() {
        return 0;
    }

    @Override
    public boolean isConnected() {
        return mIsConnected;
    }

    @Override
    public void startReceiving() {

    }

    @Override
    public void stopReceiving() {

    }

    @Override
    public String getLine() {
        int read = wfdb.getframe(samples.cast());
        if (read > 0) {
            return "EKG " + samples.getitem(0) + " " + samples.getitem(1) + " " + samples.getitem(2) + "\n";
        } else {
            return "EKG 0 0 0\n";
        }
    }

    @Override
    public void sendPacket(byte[] data) {

    }

}
