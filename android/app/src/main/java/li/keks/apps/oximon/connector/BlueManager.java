package li.keks.apps.oximon.connector;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BlueManager extends BroadcastReceiver implements ListAdapter {

    private BluetoothAdapter mBluetoothAdapter;
    private List<IDevice> mAvailableDevices;
    private List<DataSetObserver> mDataObserver = new ArrayList<>();
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            BlueManager.this.notifyDataSetChanged();
        }
    };

    public BlueManager(BluetoothAdapter bluetoothAdapter) {
        mAvailableDevices = new ArrayList<>();
        mBluetoothAdapter = bluetoothAdapter;
    }

    public List<IDevice> getAvailableDevices() {
        List<IDevice> availableDevices = new ArrayList<>();
        queryPairedDevices();
        return availableDevices;
    }

    public void startDiscovery() {
        mBluetoothAdapter.startDiscovery();
    }

    public void cancleDiscovery() {
        mBluetoothAdapter.cancelDiscovery();
    }

    private void queryPairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                mAvailableDevices.add(new BlueDevice(device));
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        // When discovery finds a device
        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            // Get the BluetoothDevice object from the Intent
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            // Add the name and address to an array adapter to show in a ListView
            mAvailableDevices.add(new BlueDevice(device));
        }
        if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
            // TODO
        }
        if (BluetoothDevice.ACTION_UUID.equals(action)) {
            // TODO
        }
    }

    private void notifyDataSetChanged() {
        for (DataSetObserver o : mDataObserver) {
            o.onChanged();
        }
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (!mDataObserver.contains(observer)) {
            mDataObserver.add(observer);
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (mDataObserver.contains(observer)) {
            mDataObserver.remove(observer);
        }
    }

    @Override
    public int getCount() {
        return mAvailableDevices.size();
    }

    @Override
    public Object getItem(int position) {
        return mAvailableDevices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mAvailableDevices.get(position).id();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = new TextView(parent.getContext());
        view.setText(mAvailableDevices.get(position).id() + "");
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return mAvailableDevices.isEmpty();
    }
}
