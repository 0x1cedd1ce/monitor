package li.keks.apps.oximon.connector;

import android.bluetooth.BluetoothDevice;

public class BlueDevice extends AbstractDevice {

    BluetoothDevice mDevice;

    public BlueDevice(BluetoothDevice device) {
        mDevice = device;
    }

    @Override
    public boolean connect() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean disconnect() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public long id() {
        return mDevice.hashCode();
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void startReceiving() {

    }

    @Override
    public void stopReceiving() {

    }

    @Override
    public String getLine() {
        return null;
    }

    @Override
    public void sendPacket(byte[] data) {

    }

}
