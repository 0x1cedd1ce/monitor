package li.keks.apps.oximon;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import li.keks.apps.oximon.connector.BlueManager;
import li.keks.apps.oximon.connector.DeviceServer;
import li.keks.apps.oximon.connector.DummyDevice;
import li.keks.apps.oximon.connector.DummyManager;
import li.keks.apps.oximon.connector.IDevice;
import li.keks.apps.oximon.connector.WifiDevice;
import li.keks.apps.oximon.connector.WifiManager;
import li.keks.apps.oximon.logger.Log;

public class DeviceSearchFragment extends Fragment {

    public static final String TAG = "DeviceSearchFragment";
    private BlueManager mBluetoothManager;
    private WifiManager mWifiManager;
    private DummyManager mDummyManager;
    private boolean mBluetoothEnabled;
    private ListView mListView;
    private DeviceServer mDeviceServer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View result;
        result = inflater.inflate(R.layout.fragment_device_search, container);
        mListView = (ListView) result.findViewById(R.id.found_devices);

        result.findViewById(R.id.btn_search_blue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });
                mBluetoothManager = ((OxiMon) getActivity()).enableBluetooth();
                findBluetoothDevices();
                mBluetoothManager.startDiscovery();
                mListView.setAdapter(mBluetoothManager);
            }
        });
        result.findViewById(R.id.btn_search_wifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWifiManager = new WifiManager(getActivity());
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.w(TAG, "onItemClick " + position);
                        IDevice device = ((WifiDevice) mWifiManager.getItem(position));
                        if (mDeviceServer != null) {
                            if (mDeviceServer.getDevice() == device) {
                                mDeviceServer.stopListening();
                                mDeviceServer.startListening();
                            } else {
                                mDeviceServer.stopListening();
                                mDeviceServer = new DeviceServer(device);
                                mDeviceServer.startListening();
                            }
                        } else {
                            mDeviceServer = new DeviceServer(device);
                            mDeviceServer.startListening();
                        }
                    }
                });
                mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.w(TAG, "onItemLongClick " + position);
                        IDevice device = ((WifiDevice) mWifiManager.getItem(position));
                        ((OxiMon) getActivity()).openECGView(device);
                        return true;
                    }
                });
                mListView.setAdapter(mWifiManager);
                mWifiManager.startDiscovery();
            }
        });

        result.findViewById(R.id.btn_search_dummy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDummyManager = new DummyManager();
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.w(TAG, "onItemClick " + position);
                        IDevice device = ((DummyDevice) mDummyManager.getItem(position));
                        if (mDeviceServer != null) {
                            if (mDeviceServer.getDevice() == device) {
                                mDeviceServer.stopListening();
                                mDeviceServer.startListening();
                            } else {
                                mDeviceServer.stopListening();
                                mDeviceServer = new DeviceServer(device);
                                mDeviceServer.startListening();
                            }
                        } else {
                            mDeviceServer = new DeviceServer(device);
                            mDeviceServer.startListening();
                        }
                    }
                });
                mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.w(TAG, "onItemLongClick " + position);
                        IDevice device = ((DummyDevice) mDummyManager.getItem(position));
                        ((OxiMon) getActivity()).openECGView(device);
                        return true;
                    }
                });
                mListView.setAdapter(mDummyManager);
            }
        });
        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBluetoothEnabled) {
            getActivity().unregisterReceiver(mBluetoothManager);
        }
    }

    public void findBluetoothDevices() {
        // Register the BroadcastReceiver
        if (mBluetoothManager != null) {
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(mBluetoothManager, filter); // Don't forget to unregister during onDestroy
            filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            getActivity().registerReceiver(mBluetoothManager, filter);
            filter = new IntentFilter(BluetoothDevice.ACTION_UUID);
            getActivity().registerReceiver(mBluetoothManager, filter);
            mBluetoothEnabled = true;
        }
    }
}
