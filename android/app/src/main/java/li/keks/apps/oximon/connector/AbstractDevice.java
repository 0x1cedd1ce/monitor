package li.keks.apps.oximon.connector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oxi on 29.12.15.
 */
public abstract class AbstractDevice implements IDevice {

    private List<DeviceListener> mDeviceListener = new ArrayList<>();

    @Override
    public void addListener(DeviceListener listener) {
        if (!mDeviceListener.contains(listener)) {
            mDeviceListener.add(listener);
        }
    }

    @Override
    public void removeListener(DeviceListener listener) {
        if (mDeviceListener.contains(listener)) {
            mDeviceListener.remove(listener);
        }
    }

    @Override
    public void notifyListener(float value) {
        for (DeviceListener listener : mDeviceListener) {
            listener.setECG(value);
        }
    }
}
