package li.keks.apps.oximon.connector;

public interface IDevice {
    boolean connect();

    boolean disconnect();

    void addListener(DeviceListener listener);

    void removeListener(DeviceListener listener);

    long id();

    boolean isConnected();

    void startReceiving();

    void stopReceiving();

    String getLine();

    void sendPacket(byte[] data);

    void notifyListener(float value);
}
