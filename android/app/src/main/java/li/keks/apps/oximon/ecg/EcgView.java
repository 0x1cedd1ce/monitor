package li.keks.apps.oximon.ecg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import li.keks.apps.oximon.connector.DeviceListener;
import li.keks.apps.oximon.connector.IDevice;

public class EcgView extends View implements DeviceListener {

    private static String TAG = "EcgView";

    private float m_mid = 0;
    private float m_height = 0;
    private float m_width = 0;
    private Paint m_LinePaint = new Paint();
    private Paint m_CurvePaint = new Paint();
    private float[] ecg_values = new float[0];
    private IDevice mDevice;
    private int mIndex = 0;

    public EcgView(Context context) {
        super(context);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        m_height = h;
        m_width = w;
        m_mid = m_height / 2.0f;
        m_LinePaint.setColor(Color.WHITE);
        m_LinePaint.setStrokeWidth(2);
        m_CurvePaint.setColor(Color.GREEN);
        m_CurvePaint.setStrokeWidth(1);
        ecg_values = new float[(int) (2 * m_width) + 1];
        mIndex = 0;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        canvas.drawLine(0, m_mid, m_width, m_mid, m_LinePaint);
        float lastX = 0;
        float lastY = ecg_values[0];
        float y;
        for (float ecg_value : ecg_values) {
            y = ecg_value;
            canvas.drawLine(lastX, lastY, lastX + 0.5f, y, m_CurvePaint);
            lastX = lastX + 0.5f;
            lastY = y;
        }
        canvas.drawLine(mIndex / 2f, 0, mIndex / 2f, m_height, m_CurvePaint);
        super.onDraw(canvas);
    }

    public void setDevice(IDevice device) {
        if (mDevice != null) {
            mDevice.removeListener(this);
        }
        mDevice = device;
        if (mDevice != null) {
            mDevice.addListener(this);
        }
    }

    @Override
    public synchronized void setECG(float value) {
        ecg_values[mIndex] = value * m_height;
        mIndex = (mIndex + 1) % ecg_values.length;
        //Log.i(TAG, value * m_height + "");
        this.postInvalidate();
    }

}
