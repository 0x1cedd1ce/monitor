package li.keks.apps.oximon.ecg;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;

import li.keks.apps.oximon.connector.IDevice;

public class EcgFragment extends Fragment {

    private EcgView m_ecgView;
    private IDevice mDevice;

    public void setDevice(IDevice device) {
        mDevice = device;
        if (m_ecgView != null) {
            m_ecgView.setDevice(device);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        HorizontalScrollView result = new HorizontalScrollView(getActivity());
        result.setFillViewport(true);
        ViewGroup.LayoutParams scrollParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        result.setLayoutParams(scrollParams);
        m_ecgView = new EcgView(getActivity());
        ViewGroup.LayoutParams logParams = new ViewGroup.LayoutParams(scrollParams);
        m_ecgView.setLayoutParams(logParams);
        result.addView(m_ecgView);
        return result;
    }

}
