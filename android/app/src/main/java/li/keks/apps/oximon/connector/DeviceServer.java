package li.keks.apps.oximon.connector;

import android.util.Base64;

import java.net.DatagramPacket;

import li.keks.apps.oximon.li.keks.apps.oximon.crypto.Crypto;
import li.keks.apps.oximon.logger.Log;

/**
 * Created by ich on 27.08.15.
 */
public class DeviceServer {

    private static String TAG = "DeviceServer";
    Thread mThread;
    private IDevice mDevice;
    private boolean mRunning = false;
    Runnable mListener = new Runnable() {
        @Override
        public void run() {
            listen();
        }
    };

    public DeviceServer(IDevice device) {
        mDevice = device;
    }

    public IDevice getDevice() {
        return mDevice;
    }

    public void startListening() {
        mThread = new Thread(mListener);
        mThread.start();
    }

    public void stopListening() {
        mDevice.sendPacket(new byte[]{'S', 'T', 'O', 'P'});
        mRunning = false;
        try {
            mThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void listen() {
        mRunning = true;
        byte[] buffer = new byte[4096];
        DatagramPacket pack = new DatagramPacket(buffer, 4096);
        mDevice.startReceiving();
        try {
            byte[] mData = new byte[]{'S', 'T', 'A', 'R', 'T'};
            mDevice.sendPacket(mData);
            while (mRunning) {
                String line = mDevice.getLine();
                if (line == null) {
                    continue;
                }
                if (line.startsWith("KEY")) {
                    byte[] encryptedData = Base64.decode(line.substring(3), Base64.DEFAULT);
                    byte[] decryptedData = Crypto.decryptKey(encryptedData);
                    Crypto.setSessionKey(decryptedData);
                } else {
                    if (Crypto.hasSessionkey()) {
                        byte[] encryptedData = Base64.decode(line, Base64.DEFAULT);
                        String decryptedData = new String(Crypto.decrypt(encryptedData));
                        if (decryptedData.startsWith("EKG")) {
                            mDevice.notifyListener(new Float(decryptedData.split(" ")[1]));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            mDevice.sendPacket(new byte[]{'S', 'T', 'O', 'P'});
            mDevice.stopReceiving();
            Log.w(TAG, "STOP");
        }
    }

}
