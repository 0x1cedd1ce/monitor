import os
import random

def fermat(p):
    if(p==2): return True
    if(not(p&1)): return False
    return pow(2,p-1,p)==1

def rabinMiller(n):
     s = n-1
     t = 0
     while s&1 == 0:
         s = s//2
         t +=1
     k = 0
     while k<128:
         a = random.randrange(2,n-1)
         #a^s is computationally infeasible.  we need a more intelligent approach
         #v = (a**s)%n
         #python's core math module can do modular exponentiation
         v = pow(a,s,n) #where values are (num,exp,mod)
         if v != 1:
             i=0
             while v != (n-1):
                 if i == t-1:
                     return False
                 else:
                     i = i+1
                     v = (v**2)%n
         k+=2
     return True

def randomInt(byte):
    return int.from_bytes(os.urandom(byte), "big")

def large_prime(bit):
    byte = bit//8
    p = randomInt(byte)
    while(not fermat(p) or not rabinMiller(p)):
        p = randomInt(byte)
    return p

def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y

def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m

def generate_keys():
    p1 = large_prime(1024)
    p2 = large_prime(1024)
    while(p1 == p2):
        p2 = large_prime(1024)
    n = p1*p2
    #print(p1,p2)
    pn = (p1-1)*(p2-1)
    e = 2**16 + 1
    while(egcd(e,pn)[0] != 1):
        e = randomInt(16)
    d = modinv(e, pn)
    #print(e,d)
    return e,d,n

if __name__ == "__main__":
    e,d,n = generate_keys()
    c = pow(100,e,n)
    m = pow(c,d,n)
    print("e = " + str(e))
    print("d = " + str(d))
    print("n = " + str(n))
